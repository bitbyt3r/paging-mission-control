import setuptools

setuptools.setup(
    name="satmon",
    version="1.0.0",
    author="Mark Murnane",
    author_email="mark@hackafe.net",
    description="Satellite Monitoring",
    long_description="Ingest satellite telemetry and create alerts when data is out of spec",
    url="https://gitlab.com/enlighten-challenge/paging-mission-control",
    packages=setuptools.find_packages(),
    python_requires=">=3.6",
    classifiers=[
        "Environment::Console",
        "License::OSI Approved::MIT License",
        "Topic::Internet::Log Analysis",
    ],
    extras_require={
        "test": [
            "pytest",
        ]
    },
    setup_requires=[
        "wheel"
    ],
    entry_points={
        "console_scripts": ["satmon=satmon.__main__:main"],
    }
)