import datetime
import random


start_time = datetime.datetime.now()
def generate_entry(anomalous=None, component=None, offset=None, satellite=None):
    if anomalous == None:
        anomalous = bool(random.getrandbits(1))
    if component == None:
        component = random.choice(["TSTAT", "BATT"])
    if offset == None:
        offset = random.random() * 3600
    if satellite == None:
        satellite = str(int(random.random() * 10000))
    timestamp = start_time + datetime.timedelta(seconds=offset)
    
    # Round to nearest millisecond
    timestamp = timestamp.replace(microsecond=int(timestamp.microsecond/1000)*1000)
    timestamp_str = timestamp.strftime("%Y%m%d %H:%M:%S.%f")[:21]
    red_high = 100
    yellow_high = 80
    yellow_low = 40
    red_low = 20
    value = random.random() * (red_high - red_low) + red_low
    if anomalous:
        if component == "TSTAT":
            value = random.random()*10 + red_high
        elif component == "BATT":
            value = red_low - random.random()*10
    entry = {
        "timestamp": timestamp,
        "satellite_id": satellite,
        "red_high_limit": float(red_high),
        "yellow_high_limit": float(yellow_high),
        "yellow_low_limit": float(yellow_low),
        "red_low_limit": float(red_low),
        "raw_value": float(value),
        "component": component,
    }
    return f"{timestamp_str}|{satellite}|{red_high}|{yellow_high}|{yellow_low}|{red_low}|{value}|{component}\n", entry


def compare_entries(entry, ref_entry):
    keys = [
        "timestamp",
        "satellite_id",
        "red_high_limit",
        "yellow_high_limit",
        "yellow_low_limit",
        "red_low_limit",
        "raw_value",
        "component"
    ]
    for key in keys:
        if entry[key] != ref_entry[key]:
            return False
    return True
