import datetime
import random
import json
import util
from satmon.detector import is_anomalous, format_alert, detect_alerts

def test_is_anomalous():
    for i in range(1000):
        anomalous = bool(random.getrandbits(1))
        _, entry = util.generate_entry(anomalous=anomalous)
        assert bool(is_anomalous(entry)) == anomalous


def test_format_alert():
    for i in range(1000):
        _, entry = util.generate_entry(anomalous=True)
        entry["severity"] = is_anomalous(entry)
        alert = format_alert(entry)
        json.dumps(alert) # Alert should be serializable
        assert type(alert) is dict
        assert "satelliteId" in alert
        assert "severity" in alert
        assert "component" in alert
        assert "timestamp" in alert
        assert type(alert["satelliteId"]) is int
        assert type(alert["severity"]) is str
        assert type(alert["component"]) is str
        assert type(alert["timestamp"]) is str
        assert str(alert["satelliteId"]) == entry["satellite_id"]
        assert alert["component"] == entry["component"]
        # Check timestamp matches. Note the last character is stripped to ignore the timezone.
        timestamp = datetime.datetime.fromisoformat(alert["timestamp"][:-1])
        assert timestamp == entry["timestamp"]


def test_detect_alerts():
    for i in range(100):
        entries = []
        alerts = []
        for satellite in range(random.randrange(10)):
            offset=0
            satellite_id = str(1000+satellite)
            for component in ["TSTAT", "BATT"]:
                num_alerts = random.randrange(10)
                for j in range(num_alerts):
                    # Put a random number of normal entries at the start
                    for k in range(random.randrange(3)):
                        _, entry = util.generate_entry(anomalous=False, offset=offset, component=component, satellite=satellite_id)
                        entries.append(entry)
                        offset += random.random()*10
                    # Put an alertable number of anomalies within a five minute window
                    _, entry = util.generate_entry(anomalous=True, offset=offset, component=component, satellite=satellite_id)
                    entries.append(entry)
                    # Keep track of the first entry, since its timestamp will be used for the alert
                    alerts.append(entry)
                    for anomaly in range(random.randrange(2, 10)):
                        temp_offset = offset + 1 + random.random()*298
                        _, entry = util.generate_entry(anomalous=True, offset=temp_offset, component=component, satellite=satellite_id)
                        entries.append(entry)
                    for anomaly in range(random.randrange(100)):
                        temp_offset = offset + 1 + random.random()*298
                        _, entry = util.generate_entry(anomalous=False, offset=temp_offset, component=component, satellite=satellite_id)
                        entries.append(entry)
                    offset += 301
        found_alerts = detect_alerts(entries)
        assert len(found_alerts) == len(alerts)
        for alert in alerts:
            assert format_alert(alert) in found_alerts
