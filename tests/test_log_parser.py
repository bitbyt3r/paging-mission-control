import tempfile
import util
from satmon.log_parser import parse_entry, parse_file


def test_parse_entry():
    for i in range(1000):
        logline, reference_entry = util.generate_entry()
        entry = parse_entry(logline)
        assert util.compare_entries(entry, reference_entry)


def test_parse_file():
    log = ""
    ref_entries = []
    for i in range(1000):
        logline, ref_entry = util.generate_entry()
        log += logline
        ref_entries.append(ref_entry)
    with tempfile.NamedTemporaryFile(delete=False) as FILE:
        FILE.write(log.encode('ASCII'))
        FILE.seek(0)
        for idx, entry in enumerate(parse_file(FILE.name)):
            assert util.compare_entries(entry, ref_entries[idx])
