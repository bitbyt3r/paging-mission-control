import satmon.__main__
import json

def test_main(capsys):
    satmon.__main__.main(["contrib/sample.log",])
    captured = capsys.readouterr()
    output = json.loads(captured.out)
    assert output
    # Checks that there is valid json output, but not 
    # whether it is correct. That is done by test_detect_alerts.
