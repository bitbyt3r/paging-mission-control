import argparse
import json
import sys
import os

from satmon.log_parser import parse_file
from satmon.detector import detect_alerts


def main(argv=None):
    parser = argparse.ArgumentParser(
        description="Process a satellite telemetry log and output alerts."
    )
    parser.add_argument("logfile", help="Path to logfile to process")
    args = parser.parse_args(argv)

    if not os.path.isfile(args.logfile):
        sys.exit(f"Could not find input file {args.logfile}")

    log_entries = parse_file(args.logfile)

    alerts = detect_alerts(log_entries)
    print(json.dumps(alerts, indent=4))


if __name__ == "__main__":
    main()

