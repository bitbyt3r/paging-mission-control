import datetime
import sys


def parse_entry(logline):
    """Takes a line from the log file and unpacks the values into a dictionary.

    Parameters:
        logline: string with optional newline containing a pipe delimited list
            of eight values

    Returns: dictionary of the form
        {
            "timestamp": datetime.datetime,
            "satellite_id": str,
            "red_high_limit": float,
            "yellow_high_limit": float,
            "yellow_low_limit": float,
            "red_low_limit": float,
            "raw_value": float,
            "component": str
        }
    """
    parts = logline.strip().split("|")
    if len(parts) != 8:
        sys.exit(f"Could not parse log entry {logline}: Wrong number of sections.")
    return {
        "timestamp": datetime.datetime.strptime(parts[0], "%Y%m%d %H:%M:%S.%f"),
        "satellite_id": parts[1],
        "red_high_limit": float(parts[2]),
        "yellow_high_limit": float(parts[3]),
        "yellow_low_limit": float(parts[4]),
        "red_low_limit": float(parts[5]),
        "raw_value": float(parts[6]),
        "component": parts[7],
    }


def parse_file(filename):
    """Takes a filename and returns an iterator over parsed entries.

       Parameters:
           filename: string path to the logfile to be parsed

       Returns: iterator that yields dictionaries as returned by parse_entry
    """
    with open(filename, "r") as LOGFILE:
        logline = LOGFILE.readline()
        while logline:
            yield parse_entry(logline)
            logline = LOGFILE.readline()
