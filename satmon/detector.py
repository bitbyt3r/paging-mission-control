def is_anomalous(entry):
    """Checks whether the log entry for the given component has an anomalous
       reading at the instant it was measured.

    Parameters:
        entry: dictionary containing a parsed logline as returned by
            log_parser.parse_entry

    Returns:
        If the value is normal this returns False. Otherwise it returns a
        string describing the error
    """
    if entry["component"] == "TSTAT":
        if entry["raw_value"] > entry["red_high_limit"]:
            return "RED HIGH"
    elif entry["component"] == "BATT":
        if entry["raw_value"] < entry["red_low_limit"]:
            return "RED LOW"
    return False


def format_alert(anomaly):
    """Formats the given anomaly object to match the specified output alert
       format. Note that this assumes that the timestamps are in UTC, which
       may or may not be a good thing. The incoming timestamps do not have
       a timezone, and the output format does.
    """
    return {
        "satelliteId": int(anomaly["satellite_id"]),
        "severity": anomaly["severity"],
        "component": anomaly["component"],
        "timestamp": anomaly["timestamp"].strftime("%Y-%m-%dT%H:%M:%S.%f")[:23] + "Z"
    }


def detect_alerts(log_entries, window=300, warn_num=3):
    """Accepts a list of log entries and returns a list of alerts.

    Parameters:
        log_entries: an iterable of dictionaries as returned by parse_file
        window: the duration in seconds during which the number of anomalous
            measurements are counted
        warn_num: the number of anomalous measurements that must be found
            within the time window to generate an alert

    Returns: A list of dictionaries, each dictionary representing an alert and
        having the form:
        {
            "satelliteId": 1000,
            "severity": "RED HIGH",
            "component": "TSTAT",
            "timestamp": "2018-01-01T23:01:38.001Z"
        }
    """
    components = {}
    alerts = []

    for entry in log_entries:
        component_name = f"{entry['satellite_id']}.{entry['component']}"
        if component_name not in components:
            components[component_name] = []

        entry["severity"] = is_anomalous(entry)
        if entry["severity"]:
            components[component_name].append(entry)

    for anomalies in components.values():
        anomalies = sorted(anomalies, key=lambda x: x["timestamp"])
        start_idx = 0
        end_idx = 0
        while start_idx < len(anomalies):
            start_time = anomalies[start_idx]["timestamp"].timestamp()
            while anomalies[end_idx]["timestamp"].timestamp() - start_time < window:
                end_idx += 1
                if end_idx == len(anomalies):
                    break
            if end_idx - start_idx >= warn_num:
                alerts.append(format_alert(anomalies[start_idx]))
                start_idx = end_idx
            else:
                start_idx += 1
    return alerts
